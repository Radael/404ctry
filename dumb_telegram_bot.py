import random
import requests
from aiogram import Bot, Dispatcher, executor, types


telegram_api_key = 'Paste telegram api here'
tenor_api_key = 'Paste tenor api here'

lmt = 20
ckey = 'my_test_app'

bot = Bot(token=telegram_api_key)
dp = Dispatcher(bot)


def get_random_tenor_gif(search_term):
    r = requests.get(
        "https://tenor.googleapis.com/v2/search?q=%s&key=%s&client_key=%s&limit=%s" % (
            search_term, tenor_api_key, ckey, lmt))
    data = r.json()
    gif = random.choice(data["results"])
    return gif['media_formats']['gif']['url']


@dp.message_handler(commands=['start'])
async def greetings(message: types.Message):
    await message.answer("my honest reaction:")
    await message.answer(get_random_tenor_gif('my honest reaction'))


@dp.message_handler()
async def honest_reaction(message: types.Message):
    await message.answer(get_random_tenor_gif(message.text))


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
