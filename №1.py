import datetime, functools

# 1
temperature = -1
day, month = datetime.date.today().day, datetime.date.today().month
print(f'Сегодня {day}/{month}. На улице {temperature} градусов')
if temperature < 0:
    print('Холодно, лучше остаться дома')

# 2
light_speed_kmh = 1079252848.80
print(f'Скорость света равна {int(light_speed_kmh // 3600)} км/с')

# 3
array = ['almonds', 'salmon', 'deez nuts']
count = len(array)
print(f'У тебя {count} продуктов, где {count} - значение переменной count.')

# 4
print(', '.join(array))
for food in array:
    print(food, end=' ')

# 5
print()
for food in array:
    if len(food) % 2 == 0:
        print(food)

# 6
def is_prime(n):
    if n < 2:
        return False
    for i in range(2, int(n**0.5) + 1):
        if n % i == 0:
            return False
    return True
print(is_prime(13))

# 7
@functools.lru_cache()
def fibonacci(n):
    return n if n < 2 else fibonacci(n-1) + fibonacci(n-2)


def fast_fibonacci(n):
    n1, n2 = 0, 1
    for i in range(n):
        n1, n2 = n2, n1 + n2
    return n1

print(fibonacci(499))
print(fast_fibonacci(10000))
